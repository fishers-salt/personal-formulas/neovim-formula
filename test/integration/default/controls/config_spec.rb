# frozen_string_literal: true

control 'neovim-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'neovim-config-file-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.local/share/nvim') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/nvim') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-config-init-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/nvim/init.vim') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
    its('content') { should include('" Dein core') }
    its('content') { should include("let $CACHE = expand('~/.cache')") }
    its('content') { should include('filetype plugin indent on') }
  end
end

control 'neovim-config-file-config-bundles-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/nvim/bundles.vim') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
    its('content') { should include('" User bundles') }
    its('content') { should include("call dein#add('vim-airline/vim-airline") }
    its('content') { should include('"" Vim plugin to dim inactive windows') }
  end
end

control 'neovim-config-file-after-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/nvim/after') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-ftdetect-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/nvim/after/ftdetect') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-ftplugin-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/nvim/after/ftplugin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-indent-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/nvim/after/indent') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-plugin-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/nvim/plugin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-ultisnippets-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/nvim/ultisnippets') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

ftdetects = %w[csv rst sls]
ftdetects.each do |ft|
  control "neovim-config-file-ftdetect-#{ft}-auser-managed" do
    title 'should match desired lines'

    describe file("/home/auser/.config/nvim/after/ftdetect/#{ft}.vim") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0644' }
      its('content') { should include('" Your changes will be overwritten.') }
      its('content') { should include("filetype=#{ft}") }
    end
  end
end

## ftplugin
ftplugins = {
  c: 'formatoptions=cqroj',
  dot: 'formatoptions+=cqroj',
  go: 'let b:ale_fix_on_save = 1',
  javascript: "let b:ale_fixers = ['prettier', 'eslint']",
  markdown: '" Table mode',
  perl: 'setlocal textwidth=80 colorcolumn=80',
  python: 'Wrap at 72 chars for comments.',
  rst: 'setlocal spell spelllang=en_gb',
  rtv: 'setlocal spell spelllang=en_gb',
  ruby: 'setlocal colorcolumn=80',
  sh: 'setlocal colorcolumn=88',
  text: 'setlocal spell spelllang=en_gb',
  tex: 'setlocal spell spelllang=en_gb',
  yaml: 'setlocal textwidth=80'
}

ftplugins.each_pair do |plugin, teststring|
  control "neovim-config-file-ftplugin-#{plugin}-auser-managed" do
    title 'should match desired lines'

    describe file("/home/auser/.config/nvim/after/ftplugin/#{plugin}.vim") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0644' }
      its('content') { should include('" Your changes will be overwritten.') }
      its('content') { should include(teststring) }
    end
  end
end

## indent
indents = {
  go: 'setlocal tabstop=4',
  'html.handlebars': 'setlocal tabstop=2',
  markdown: 'setlocal tabstop=2',
  rst: 'setlocal expandtab',
  ruby: 'setlocal expandtab'
}

indents.each_pair do |indent, teststring|
  control "neovim-config-file-#{indent}-auser-managed" do
    title 'should match desired lines'

    describe file("/home/auser/.config/nvim/after/indent/#{indent}.vim") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0644' }
      its('content') { should include('" Your changes will be overwritten.') }
      its('content') { should include(teststring) }
    end
  end
end
## plugins
plugins = {
  airline: '" Airline',
  ale: '" ALE',
  anyfold: '" Anyfold',
  deoplete: '" Deoplete',
  easytags: '" Easytags',
  fugitive: '" Git',
  fzf: '" FZF',
  gitgutter: '" Gitgutter',
  grepper: '" Grepper',
  gruvbox: '" Gruvbox',
  gundo: '" Undo',
  indentline: '" indentLine',
  nerdtree: '" Nerdtree',
  promptline: '" Promptline',
  riv: '" Riv',
  tagbar: '" Tagbar',
  ultisnips: '" Ultisnips',
  'vim-test': '" Testing',
  vimtex: '" Vimtex',
  vimwiki: 'let test_wiki = {}'
}

plugins.each_pair do |plugin, teststring|
  control "neovim-config-file-plugin-#{plugin}-auser-managed" do
    title 'should match desired lines'

    describe file("/home/auser/.config/nvim/plugin/#{plugin}.vim") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0644' }
      its('content') { should include('"  Your changes will be overwritten.') }
      its('content') { should include(teststring) }
    end
  end
end

## ultisnippets
ultisnippets = {
  all: 'def get_comment_format()',
  c: 'snippet wh "while loop"',
  go: 'snippet magicdate "Magic date comment" b',
  htmldjango: 'snippet	extpar "" bi',
  html: 'snippet html5 "HTML5 Template"',
  markdown: 'snippet dl "Definition list" b',
  moin: 'snippet update "Install upgrades" b',
  php: 'snippet flashbag',
  python: 'snippet py2input "Backwards compatible input" b',
  sh: 'snippet getscriptdir "Get current directory of running script"',
  todo: 'snippet todo "Todo item" b',
  vim: 'snippet wh'
}

ultisnippets.each_pair do |ultisnippet, teststring|
  control "neovim-config-file-ultisnippet-#{ultisnippet}-auser-managed" do
    title 'should match desired lines'

    describe file("/home/auser/.config/nvim/ultisnippets/#{ultisnippet}.snippets") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0644' }
      its('content') { should include('# Your changes will be overwritten.') }
      its('content') { should include(teststring) }
    end
  end
end

control 'neovim-config-file-zsh-env-include-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/env_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'neovim-config-file-editor-zsh-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/env_includes/editor.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('if command -v nvim > /dev/null; then') }
    its('content') { should include('export GIT_EDITOR=${PREFERRED_EDITOR}') }
  end
end
