# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch'
    %w[neovim python-pynvim]
  else
    %w[neovim python3-neovim]
  end

finger = system.platform[:finger].split('.').first.to_s
packages = %w[neovim] if finger == 'debian-9'

packages.each do |pkg|
  control "neovim-package-install-pkg-#{pkg}-installed" do
    title "#{pkg} should be installed"

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
