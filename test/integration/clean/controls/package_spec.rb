# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch'
    %w[neovim python-pynvim]
  else
    %w[neovim python3-neovim]
  end

finger = system.platform[:finger].split('.').first.to_s
packages = %w[neovim] if finger == 'debian-9'

packages.each do |pkg|
  control "neovim-package-clean-pkg-#{pkg}-removed" do
    title "#{pkg} should not be installed"

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
