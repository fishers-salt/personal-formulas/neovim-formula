# frozen_string_literal: true

control 'neovim-config-clean-editor-zsh-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/env_includes/editor.zsh') do
    it { should_not exist }
  end
end

control 'neovim-config-file-ultisnippets-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/nvim/ultisnippets') do
    it { should_not exist }
  end
end

control 'neovim-config-file-plugin-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/nvim/plugin') do
    it { should_not exist }
  end
end

control 'neovim-config-file-indent-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/nvim/after/indent') do
    it { should_not exist }
  end
end

control 'neovim-config-file-ftplugin-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/nvim/after/ftplugin') do
    it { should_not exist }
  end
end

control 'neovim-config-file-ftdetect-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/nvim/after/ftdetect') do
    it { should_not exist }
  end
end

control 'neovim-config-file-after-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/nvim/after') do
    it { should_not exist }
  end
end

control 'neovim-config-file-config-bundles-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/nvim/bundles.vim') do
    it { should_not exist }
  end
end

control 'neovim-config-file-config-init-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/nvim/init.vim') do
    it { should_not exist }
  end
end

control 'neovim-config-file-dein-cache-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.cache/dein') do
    it { should_not exist }
  end
end

control 'neovim-config-file-dein-data-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.local/share/dein') do
    it { should_not exist }
  end
end

control 'neovim-config-file-config-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/nvim') do
    it { should_not exist }
  end
end

control 'neovim-config-file-data-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.local/share/nvim') do
    it { should_not exist }
  end
end
