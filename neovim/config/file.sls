# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as neovim with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('neovim-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_neovim', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

neovim-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

neovim-config-file-data-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.local/share/nvim
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present

neovim-config-file-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/nvim
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present

neovim-config-file-config-init-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/nvim/init.vim
    - source: {{ files_switch([
                   name ~ '-init.vim.tmpl', 'init.vim.tmpl'],
                 lookup='neovim-config-file-config-init-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - neovim-config-file-config-dir-{{ name }}-managed

neovim-config-file-config-bundles-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/nvim/bundles.vim
    - source: {{ files_switch([
                   name ~ '-bundles.vim.tmpl', 'bundles.vim.tmpl'],
                 lookup='neovim-config-file-config-bundles-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - neovim-config-file-config-dir-{{ name }}-managed

neovim-config-file-after-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/nvim/after
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present
      - neovim-config-file-config-dir-{{ name }}-managed

neovim-config-file-ftdetect-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/nvim/after/ftdetect
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present
      - neovim-config-file-config-dir-{{ name }}-managed
      - neovim-config-file-after-dir-{{ name }}-managed

neovim-config-file-ftplugin-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/nvim/after/ftplugin
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present
      - neovim-config-file-config-dir-{{ name }}-managed
      - neovim-config-file-after-dir-{{ name }}-managed

neovim-config-file-indent-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/nvim/after/indent
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present
      - neovim-config-file-config-dir-{{ name }}-managed
      - neovim-config-file-after-dir-{{ name }}-managed

neovim-config-file-plugin-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/nvim/plugin
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present
      - neovim-config-file-config-dir-{{ name }}-managed

neovim-config-file-ultisnippets-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/nvim/ultisnippets
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present
      - neovim-config-file-config-dir-{{ name }}-managed

{% set user_config = neovim.get(name, {}) %}
{% set ftdetect_files = user_config.get('ftdetect_files', None) %}
{% if ftdetect_files == None %}
{% set ftdetect_files = neovim.get('ftdetect_files', []) %}
{% endif %}

{% for file in ftdetect_files %}
neovim-config-file-ftdetect-{{ file }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/nvim/after/ftdetect/{{ file }}.vim
    - source: {{ files_switch([
                   'ftdetect/' ~ name ~ '-' ~ file ~ '.vim.tmpl',
                   'ftdetect/' ~ file ~ '.vim.tmpl'],
                 lookup='neovim-config-file-ftdetect-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
    - require:
      - neovim-config-file-ftdetect-dir-{{ name }}-managed
{% endfor %}

{% set user_config = neovim.get(name, {}) %}
{% set ftplugin_files = user_config.get('ftplugin_files', None) %}
{% if ftplugin_files == None %}
{% set ftplugin_files = neovim.get('ftplugin_files', []) %}
{% endif %}

{% for file in ftplugin_files %}
neovim-config-file-ftplugin-{{ file }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/nvim/after/ftplugin/{{ file }}.vim
    - source: {{ files_switch([
                   'ftplugin/' ~ name ~ '-' ~ file ~ '.vim.tmpl',
                   'ftplugin/' ~ file ~ '.vim.tmpl'],
                 lookup='neovim-config-file-ftplugin-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
        neovim: {{ neovim }}
    - require:
      - neovim-config-file-ftplugin-dir-{{ name }}-managed
{% endfor %}

{% set user_config = neovim.get(name, {}) %}
{% set indent_files = user_config.get('indent_files', None) %}
{% if indent_files == None %}
{% set indent_files = neovim.get('indent_files', []) %}
{% endif %}

{% for file in indent_files %}
neovim-config-file-indent-{{ file }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/nvim/after/indent/{{ file }}.vim
    - source: {{ files_switch([
                   'indent/' ~ name ~ '-' ~ file ~ '.vim.tmpl',
                   'indent/' ~ file ~ '.vim.tmpl'],
                 lookup='neovim-config-file-indent-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
    - require:
      - neovim-config-file-indent-dir-{{ name }}-managed
{% endfor %}

{% set user_config = neovim.get(name, {}) %}
{% set plugin_files = user_config.get('plugin_files', None) %}
{% if plugin_files == None %}
{% set plugin_files = neovim.get('plugin_files', []) %}
{% endif %}

{% for file in plugin_files %}
neovim-config-file-plugin-{{ file }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/nvim/plugin/{{ file }}.vim
    - source: {{ files_switch([
                   'plugin/' ~ name ~ '-' ~ file ~ '.vim.tmpl',
                   'plugin/' ~ file ~ '.vim.tmpl'],
                 lookup='neovim-config-file-plugin-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        neovim: {{ neovim }}
    - require:
      - neovim-config-file-plugin-dir-{{ name }}-managed
{% endfor %}

{% set user_config = neovim.get(name, {}) %}
{% set ultisnippets_files = user_config.get('ultisnippets_files', None) %}
{% if ultisnippets_files == None %}
{% set ultisnippets_files = neovim.get('ultisnippets_files', []) %}
{% endif %}

{% for file in ultisnippets_files %}
neovim-config-file-ultisnippets-{{ file }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/nvim/ultisnippets/{{ file }}.snippets
    - source: {{ files_switch([
                   'ultisnippets/' ~ name ~ '-' ~ file ~ '.snippets.tmpl',
                   'ultisnippets/' ~ file ~ '.snippets.tmpl'],
                 lookup='neovim-config-file-ultisnippets-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
    - require:
      - neovim-config-file-ultisnippets-dir-{{ name }}-managed
{% endfor %}

{%- set preferred_editor = user.get('preferred_editor', None) -%}
{% if preferred_editor == "neovim" %}

neovim-config-file-zsh-env-include-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/env_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - neovim-config-file-user-{{ name }}-present

neovim-config-file-editor-zsh-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/env_includes/editor.zsh
    - source: {{ files_switch([
                   'zsh/' ~ name ~ 'editor.zsh.tmpl',
                   'zsh/editor.zsh.tmpl'],
                 lookup='neovim-config-file-editor-zsh-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - neovim-config-file-zsh-env-include-{{ name }}-managed
{% endif %}

{% endif %}
{% endfor %}
{% endif %}
