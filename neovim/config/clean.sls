# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as neovim with context %}

{% if salt['pillar.get']('neovim-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_neovim', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

{%- set preferred_editor = user.get('preferred_editor', None) -%}
{% if preferred_editor == "neovim" %}

neovim-config-file-editor-zsh-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/zsh/env_includes/editor.zsh
{% endif %}

neovim-config-file-ultisnippets-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/ultisnippets

neovim-config-file-plugin-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/plugin

neovim-config-file-indent-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/after/indent

neovim-config-file-ftplugin-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/after/ftplugin

neovim-config-file-ftdetect-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/after/ftdetect

neovim-config-file-after-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/after

neovim-config-file-config-bundles-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/bundles.vim

neovim-config-file-config-init-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim/init.vim

neovim-config-file-dein-cache-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.cache/dein

neovim-config-file-dein-data-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.local/share/dein

neovim-config-file-config-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/nvim

neovim-config-file-data-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.local/share/nvim
{% endif %}
{% endfor %}
{% endif %}
