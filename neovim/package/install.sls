# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as neovim with context %}

neovim-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ neovim.pkgs }}
